(function($) {
  $('#drgoFireFishUpdate').on('click', function(e) {
    var $this = $(this),
        $return = $this.next('#drgoFireFishReturn');

    if($this.attr('disabled')) { return false; }

    if($return.length < 1) {
        $this.after(' <strong id="drgoFireFishReturn" aria-live="assertive" role="alert"></strong>');
        $return = $this.next('#drgoFireFishReturn');
    }

    $this.attr('disabled', true);
    $return.html('Working…');

    $.ajax({
      'url'      : window.ajaxurl,
      'data'     : {
        'action' : 'drgo_firefish_update',
      },
      'method'   : 'post',
      'cache'    : false,
      'datatype' : 'json',
      'complete' : (function(e, status) {
        if(status === 'success') {
          $return.html('Posts Updated');
        } else {
          $return.html('An unknown error occurred. Please try again later.');
        }

        $this.removeAttr('disabled');
      }),
    });
  });
})(jQuery);
