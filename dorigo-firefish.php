<?php
/*
Plugin Name: Firefish
Plugin URI: https://bitbucket.org/lewisdorigo/firefish
Description: Gets jobs from a FireFish job site, and adds them as into wordpress.
Version: 1.1.3
Author: Lewis Dorigo
Author URI: https://dorigo.co
Copyright: Lewis Dorigo
*/


if(!defined('ABSPATH')) {
    exit;
}

define('DRGO_FIREFISH_PLUGIN', __FILE__);

if(file_exists(__DIR__.'/vendor/autoload.php')) {
    require_once(__DIR__.'/vendor/autoload.php');
}

error_reporting(E_ALL);
ini_set('display_errors', 1);

global $firefish;
$firefish = \Dorigo\FireFish\Base::Init();

function get_firefish_permalink($post_id = null) {
    global $firefish;

    return $firefish->getPermalink($post_id);
}

function get_firefish_apply_link($post_id = null) {
    global $firefish;

    return $firefish->getApplicationLink($post_id);
}

function get_firefish_meta($post_id = null, $name = 'reference_number') {
    global $firefish;

    return $firefish->getMeta($post_id, $name);
}