<?php namespace Dorigo\FireFish;

class PostType {
    private static $instance;

    private $postType = 'firefish_job';
    private $capabilities = [
        'edit_firefish_job'             => true,
        'edit_published_firefish_jobs'  => true,
        'edit_others_firefish_jobs'     => true,
        'read_firefish_job'             => true,
        'delete_firefish_job'           => false,

        'edit_firefish_jobs'            => true,
        'edit_others_firefish_jobs'     => true,
        'publish_firefish_jobs'         => false,
        'read_private_firefish_jobs'    => true,
        'create_firefish_jobs'          => false,
    ];

    private $defaultTaxonomy = [
        'hierarchical'          => false,
        'show_ui'               => false,
        'show_in_quick_edit'    => false,
        'show_tagcloud'         => false,
        'show_in_nav_menus'     => false,
        'show_admin_column'     => false,
        'query_var'             => false,
        'rewrite'               => false,
    ];

    public static function Init() {
        if(is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct() {
        add_action("init", [$this, "add"]);

        add_filter("Dorigo/FireFish/Taxonomy/type=location_area", [$this, "showAdminColumn"], 0, 1);
        add_filter("Dorigo/FireFish/Taxonomy/type=role", [$this, "showAdminColumn"], 0, 1);
        add_filter("Dorigo/FireFish/Taxonomy/type=discipline", [$this, "showAdminColumn"], 0, 1);
        add_filter("Dorigo/FireFish/Taxonomy/type=job_type", [$this, "showAdminColumn"], 0, 1);
    }

    public function add() {
        $this->postType();

        $this->location();
        $this->locationArea();
        $this->role();
        $this->discipline();
        $this->jobType();
    }

    public function showAdminColumn($object) {
        $object["show_admin_column"] = true;
        return $object;
    }

    private function postType() {
        $this->addCapabilities();

        $slug = get_field("{$this->postType}_archive", 'option') ? get_field("{$this->postType}_archive", 'option') : $this->postType;

        register_post_type($this->postType, [
            'labels' => [
                'name' => __('Jobs'),
                'singular_name' => __('Job'),
                'menu_name' => __('Jobs'),
                'name_admin_bar' => __('Job'),
                'add_new' => __('Add New'),
                'add_new_item' => __('Add New'),
                'search_items' => __('Search Jobs'),
                'new_item' => __('New Job'),
                'edit_item' => __('Edit'),
                'view_item' => __('View Job'),
                'not_found' => __('No jobs found.'),
                'all_items' => __('All Jobs'),
            ],
            'public' => true,
            'publicly_queriable' => true,
            'show_ui' => true,
            'exclude_from_search' => false,
            'show_in_nav_menus' => true,
            'show_in_admin_bar' => true,
            'has_archive' => true,
            'hierarchical' => false,
            'menu_position' => 20,
            'menu_icon' => 'dashicons-businessman',
            'supports' => apply_filters('Dorigo\FireFish\PostType\Supports', [
                'title',
                'editor',
                'excerpt',
            ]),
            'capabilities' => [
                'edit_post'            => "edit_firefish_job",
                'read_post'            => "read_firefish_job",
                'delete_post'          => "delete_firefish_job",

                'edit_posts'           => "edit_firefish_jobs",
                'edit_others_posts'    => "edit_others_firefish_jobs",
                'publish_posts'        => "publish_firefish_jobs",
                'read_private_posts'   => "read_private_firefish_jobs",
                'create_posts'         => "create_firefish_jobs",
                'delete_posts'         => "delete_firefish_job",

                'edit_published_posts' => "edit_published_firefish_jobs",
                'delete_others_posts'  => "delete_firefish_job",
            ],
            'rewrite' => ($slug ? [
                'slug' => $slug,
                'with_front' => false,
                'feed' => false,
                'pages' => false,
            ] : null),
        ]);
    }

    private function addCapabilities() {
        $roles = wp_roles();

        foreach($this->capabilities as $name => $value) {

            $roles->add_cap('administrator', $name, $value);
            $roles->add_cap('editor', $name, $value);

        }
    }

    public function getPostType() {
        return $this->postType;
    }

    private function jobType() {

        $object = apply_filters('Dorigo/FireFish/Taxonomy', $this->defaultTaxonomy, 'job_type');
        $object = apply_filters('Dorigo/FireFish/Taxonomy/type=job_type', array_merge([
            'labels' => [
                'name'                       => 'Job Types',
                'singular_name'              => 'Job Type',
                'menu_name'                  => 'Job Types',
                'all_items'                  => 'All Job Types',
                'edit_item'                  => 'Edit Job Type',
                'view_item'                  => 'View Job Type',
                'update_item'                => 'Update Job Type',
                'add_new_item'               => 'Add New Job Type',
                'new_item_name'              => 'New Job Type Name',
                'parent_item'                => 'Job Type Parent',
                'parent_item_colon'          => 'Job Type Parent:',
                'search_items'               => 'Search Job Types',
                'popular_items'              => 'Popular Job Types',
                'separate_items_with_commas' => 'Separate job types with commas',
                'add_or_remove_items'        => 'Add or remove job types',
                'choose_from_most_used'      => 'Choose from the most used job types',
                'not_found'                  => 'No job types found',
            ],
        ], $object), 'job_type');

        register_taxonomy("{$this->postType}_job_type", [$this->postType], $object);
    }

    private function discipline() {
        $object = apply_filters('Dorigo/FireFish/Taxonomy', $this->defaultTaxonomy, 'discipline');
        $object = apply_filters('Dorigo/FireFish/Taxonomy/type=discipline', array_merge([
            'labels' => [
                'name'                       => 'Disciplines',
                'singular_name'              => 'Discipline',
                'menu_name'                  => 'Disciplines',
                'all_items'                  => 'All Disciplines',
                'edit_item'                  => 'Edit Discipline',
                'view_item'                  => 'View Discipline',
                'update_item'                => 'Update Discipline',
                'add_new_item'               => 'Add New Discipline',
                'new_item_name'              => 'New Job Discipline',
                'parent_item'                => 'Discipline Parent',
                'parent_item_colon'          => 'Discipline Parent:',
                'search_items'               => 'Search Disciplines',
                'popular_items'              => 'Popular Disciplines',
                'separate_items_with_commas' => 'Separate disciplines with commas',
                'add_or_remove_items'        => 'Add or remove disciplines',
                'choose_from_most_used'      => 'Choose from the most used disciplines',
                'not_found'                  => 'No disciplines found',
            ],
        ], $object), 'discipline');

        register_taxonomy("{$this->postType}_discipline", [$this->postType], $object);
    }

    private function role() {
        $object = apply_filters('Dorigo/FireFish/Taxonomy', $this->defaultTaxonomy, 'role');
        $object = apply_filters('Dorigo/FireFish/Taxonomy/type=role', array_merge([
            'labels' => [
                'name'                       => 'Roles',
                'singular_name'              => 'Role',
                'menu_name'                  => 'Roles',
                'all_items'                  => 'All Roles',
                'edit_item'                  => 'Edit Role',
                'view_item'                  => 'View Role',
                'update_item'                => 'Update Role',
                'add_new_item'               => 'Add New Role',
                'new_item_name'              => 'New Role',
                'parent_item'                => 'Role Parent',
                'parent_item_colon'          => 'Role Parent:',
                'search_items'               => 'Search Roles',
                'popular_items'              => 'Popular Roles',
                'separate_items_with_commas' => 'Separate roles with commas',
                'add_or_remove_items'        => 'Add or remove roles',
                'choose_from_most_used'      => 'Choose from the most used roles',
                'not_found'                  => 'No roles found',
            ],
        ], $object), 'role');

        register_taxonomy("{$this->postType}_role", [$this->postType], $object);
    }

    private function location() {
        $object = apply_filters('Dorigo/FireFish/Taxonomy', $this->defaultTaxonomy, 'location');
        $object = apply_filters('Dorigo/FireFish/Taxonomy/type=location', array_merge([
            'labels' => [
                'name'                       => 'Locations',
                'singular_name'              => 'Location',
                'menu_name'                  => 'Locations',
                'all_items'                  => 'All Locations',
                'edit_item'                  => 'Edit Location',
                'view_item'                  => 'View Location',
                'update_item'                => 'Update Location',
                'add_new_item'               => 'Add New Location',
                'new_item_name'              => 'New Location',
                'parent_item'                => 'Location Parent',
                'parent_item_colon'          => 'Location Parent:',
                'search_items'               => 'Search Locations',
                'popular_items'              => 'Popular Locations',
                'separate_items_with_commas' => 'Separate locations with commas',
                'add_or_remove_items'        => 'Add or remove locations',
                'choose_from_most_used'      => 'Choose from the most used locations',
                'not_found'                  => 'No locations found',
            ],
        ], $object), 'location');

        register_taxonomy("{$this->postType}_location", [$this->postType], $object);
    }

    private function locationArea() {
        $object = apply_filters('Dorigo/FireFish/Taxonomy', $this->defaultTaxonomy, 'location_area');
        $object = apply_filters('Dorigo/FireFish/Taxonomy/type=location_area', array_merge([
            'labels' => [
                'name'                       => 'Location Areas',
                'singular_name'              => 'Location Area',
                'menu_name'                  => 'Location Areas',
                'all_items'                  => 'All Location Areas',
                'edit_item'                  => 'Edit Location Area',
                'view_item'                  => 'View Location Area',
                'update_item'                => 'Update Location Area',
                'add_new_item'               => 'Add New Location Area',
                'new_item_name'              => 'New Location Area',
                'parent_item'                => 'Location Area Parent',
                'parent_item_colon'          => 'Location Area Parent:',
                'search_items'               => 'Search Location Areas',
                'popular_items'              => 'Popular Location Areas',
                'separate_items_with_commas' => 'Separate location areas with commas',
                'add_or_remove_items'        => 'Add or remove location areas',
                'choose_from_most_used'      => 'Choose from the most used location area',
                'not_found'                  => 'No location area found',
            ],
        ], $object), 'location_area');

        register_taxonomy("{$this->postType}_location_area", [$this->postType], $object);
    }
}
