<?php namespace Dorigo\FireFish;

class Settings {
    private static $instance;

    private $title = 'FireFish Settings';
    private $menuLabel = 'FireFish';
    private $name = 'drgo_firefish';
    private $settings = [
        [
            'type'  => 'url',
            'name'  => 'drgo_firefish_site_url',
            'label' => 'Site URL'
        ],
    ];

    public static function Init() {
        if(is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct() {
        if(is_admin()) {
            add_action('admin_menu', [$this, 'menu']);
            add_action('admin_init', [$this, 'registerSettings']);
        }
    }

    public function menu() {
        add_options_page($this->title, $this->menuLabel, 'manage_options',$this->name, [$this, 'settingsOutput']);
    }

    public function settings() {
        return apply_filters('Dorigo/FireFish/Settings', $this->settings);
    }

    public function registerSettings() {
        $settings = $this->settings();

        foreach($settings as $setting) {
            register_setting($this->name, $setting['name']);
        }
    }

    public function settingsOutput() {
        $settings = $this->settings();

        require dirname(DRGO_FIREFISH_PLUGIN).'/templates/settings.php';
    }

    public function getOptions() {
        $options = new \StdClass;

        foreach($this->settings() as $setting) {
            $key = $setting['name'];
            $value = get_option($setting['name']) ?: false;

            $attr = preg_replace("/^{$this->name}_/", '', $key);

            $options->{$attr} = $value;
        }

        return $options;
    }

    public function getOption($name = null) {
        $options = $this->getOptions();

        if($name) {
            return isset($options->{$name}) ? $options->{$name} : false;
        } else {
            return $options;
        }
    }
}