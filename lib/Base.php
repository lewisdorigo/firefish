<?php namespace Dorigo\FireFish;

class Base {
    private static $instance;

    private $cronAction = 'drgo_firefish_update';

    private $settings;
    private $api;
    private $postType;

    public static function Init() {
        if(is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct() {
        $this->postType = PostType::Init();
        $this->settings = Settings::Init();
        $this->api = API::Init();

        $this->addHooks();
    }

    private function addHooks() {

        register_activation_hook(DRGO_FIREFISH_PLUGIN, [$this,'activate']);
        register_deactivation_hook(DRGO_FIREFISH_PLUGIN, [$this,'deactivate']);

        add_action($this->cronAction, [$this, 'getListings']);

        add_filter('pre_get_posts', [$this, 'removeClosed']);

        add_action('admin_enqueue_scripts', [$this, 'admin_js']);

        add_action("wp_ajax_{$this->cronAction}", function() {
            do_action($this->cronAction);

            header('Content-Type: application/json');

            echo json_encode([
                'status' => 'ok',
                'message' => 'success'
            ]);

            die();
        });

    }

    public function admin_js() {
        wp_enqueue_script('drgo_firefish_js', plugin_dir_url(DRGO_FIREFISH_PLUGIN).'/assets/admin-js.js', ['jquery-core'], null, true);
    }

    public function activate() {
        wp_schedule_event(time(), 'hourly', $this->cronAction, []);
    }

    public function deactivate() {
        wp_clear_scheduled_hook($this->cronAction, []);
    }

    public function getListings() {
        return $this->api->getListings();
    }

    public function getOption($name = null) {
        return $this->settings->getOption($name);
    }

    public function removeClosed($query) {
        if($query->get('post_type') !== $this->postType->getPostType()) { return $query; }

        $meta = $query->get('meta_query') ?: [];

        if(!empty($meta)) {
            $meta = [
                'relation' => 'AND',
                $meta
            ];
        }

        $meta[] = [
            'type' => 'DATETIME',
            'key' => '_drgo_firefish_closing_date',
            'compare' => '>=',
            'value' => date('Y-m-d H:i:s'),
        ];

        $query->set('meta_query', $meta);
    }

    public function getMeta($post_id = null, $name = 'reference_number') {
        $post = get_post($post_id);

        return get_post_meta($post->ID, "_drgo_firefish_{$name}", true);
    }

    public function getPermalink($post_id = null) {
        return $this->getMeta($post_id, 'link');
    }

    public function getApplicationLink($post_id = null) {
        $post = get_post($post_id);

        $link = $this->getMeta($post->ID, 'link');
        return preg_replace('/\.aspx$/', '/apply.aspx', $link);

    }
}