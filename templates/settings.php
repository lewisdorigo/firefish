<div class="wrap">
    <h2><?= $this->title; ?></h2>

    <form action="options.php" method="post">
        <?php settings_fields($this->name); ?>

        <table class="form-table">
            <tbody>
                <?php foreach($settings as $setting): ?>
                    <tr>
                        <th scope="row">
                            <label for="<?=$setting['name']; ?>"><?= $setting['label']; ?></label>
                        </th>

                        <td>
                            <input type="<?= $setting['type']; ?>"
                                   name="<?= $setting['name']; ?>"
                                     id="<?= $setting['name']; ?>"
                                  value="<?= get_option($setting['name']); ?>"
                                  class="regular-text ltr">
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <?php submit_button(); ?>

        <hr>

        <p>
            <a href="#" class="button" id="drgoFireFishUpdate">Update Listings</a>
        </p>
    </form>
</div>
