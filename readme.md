#FireFish for WordPress

This plugin takes pulls updates from a specified [FireFish](https://www.firefishsoftware.com) job site, and loads them into a custom post type: `firefish_job`.

When the plugin is first activated, it will create a `wp_cron` job to update job listings every hour.

If this plugin is set as an `mu-plugin`, this cron job may not be activated, resulting in blog posts not updating properly.

Job listings created in WordPress by this plugin will include the following post meta values:

- `_drgo_firefish_reference_number`
- `_drgo_firefish_company_reference`
- `_drgo_firefish_link`
- `_drgo_firefish_closing_date`
- `_drgo_firefish_job_type`
- `_drgo_firefish_discipline`
- `_drgo_firefish_role`
- `_drgo_firefish_location_area`
- `_drgo_firefish_location`
- `_drgo_firefish_remuneration`
- `_drgo_firefish_lat_lng`

There are also taxonomies, to allow for easier filtering:

- `firefish_job_job_type`
- `firefish_job_discipline`
- `firefish_job_role`
- `firefish_job_location_area`
- `firefish_job_location`
- `firefish_job_remuneration`

There are also three functions:

- `get_firefish_permalink()` returns the link to the job listing on the FireFish job site
- `get_firefish_apply_link()` returns the link to the application page the FireFish job site.
- `get_firefish_meta($post_id, $meta_name)` returns the meta value for the specified post. Using this function, you can _exclude_ `_drgo_firefish_` from the post meta key. For example, to get the reference number, for a post, you could do `get_firefish_meta($post->ID, 'reference_number')`.

This plugin does not process applications through Firefish — it only adds jobs to WordPress so they can be indexed and included as part of the main site.

--------

## Getting Latitude and Longitude

To get the `_drgo_firefish_lat_lng` of the job listing, you first need need to set an [API Key](https://console.developers.google.com/) using the `Dorigo\FireFish\GoogleAPI` filter:

```
add_filter('Dorigo\FireFish\GoogleAPI', function($api_key) {
    return 'API_KEY';
});
```

This API Key should have the Google Geocoding API enabled. Be aware that — depending on the number of job listings — this _may_ be looking up dozens or hundreds of locations, so be wary of the [Geocoding API Usage Limits](https://developers.google.com/maps/documentation/geocoding/usage-limits)

Once an API Key is present, during the next update the plugin will now get the latitude and longitude using the Google Places API.

The `_drgo_firefish_lat_lng` contains an array with the latitude and longitude:

```
[
    'lat' => 51.386322,
    'lng' => 0.551438
]
```

By default, the plugin assumes a UK bias when looking up locations, but you can change this using the `Dorigo\FireFish\GoogleRegion` filter. This sets the region parameter, as outlined in the [Geocoding API documentation](https://developers.google.com/maps/documentation/geocoding/intro#RegionCodes).

